import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue2";

// https://vitejs.dev/config/
export default defineConfig({
  base: "./",
  plugins: [vue()],
  define: {
    LAST_BUILT: JSON.stringify(new Date().toISOString()),
    GITLAB_ANALYTICS_ID: JSON.stringify(
      process.env.GITLAB_ANALYTICS_ID || false
    ),
    GITLAB_ANALYTICS_HOST: JSON.stringify(
      process.env.GITLAB_ANALYTICS_HOST || false
    ),
  },
});
