import { dirname, join } from "node:path";
import { copyFile, mkdir, stat, writeFile } from "node:fs/promises";

import axios from "axios";
import micromatch from "micromatch";
import {
  setupGitLabAPI,
  GitLabPagedAPIIterator,
} from "gitlab-api-async-iterator";

import { asyncPool } from "./async-pool.js";
import { ROOT_DIR } from "./constants.js";

const GitLabAPI = setupGitLabAPI(axios);

export async function fileExists(path) {
  try {
    await stat(path);
    return true;
  } catch (e) {
    return false;
  }
}

export const downloadFileIfNecessaryFactory = (cacheDir, targetDir) => async (
  file
) => {
  const { type, name, path, id, downloadUrl } = file;

  if (type === "blob") {
    const cachePath = join(cacheDir, id);
    const targetPath = join(targetDir, path);
    await mkdir(dirname(targetPath), { recursive: true });
    if (await fileExists(cachePath)) {
      console.log(`Loading ${path} via Cache`);
      await copyFile(cachePath, targetPath);
    } else {
      console.log(`Loading ${path} via API`);
      const { data } = await GitLabAPI.get(downloadUrl);
      const buffer = Buffer.from(data.content, "base64");
      await writeFile(targetPath, buffer, "utf-8");
      await writeFile(cachePath, buffer, "utf-8");
    }

    return { name };
  }

  return [];
};

export async function recurseRepoAndDownload(
  {
    projectId = null,
    startTree = null,
    allowedTrees = ["**/*"],
    allowedBlobs = ["**/*"],
    ignore = null,
    targetDir = ROOT_DIR,
  } = null
) {
  const BLOB_CACHE_PATH = join(targetDir, "blob_cache");
  await mkdir(BLOB_CACHE_PATH, { recursive: true });

  const stack = [...startTree];
  const releasePostItems = [];

  do {
    const path = stack.shift();

    const fileIterator = new GitLabPagedAPIIterator(
      GitLabAPI,
      `/projects/${projectId}/repository/tree`,
      { per_page: 100, path }
    );

    for await (const entry of fileIterator) {
      const { type, path, id } = entry;

      if (
        Array.isArray(ignore) &&
        ignore.length &&
        micromatch.isMatch(path, ignore)
      ) {
        continue;
      }

      if (type === "blob" && micromatch.isMatch(path, allowedBlobs)) {
        console.log(`Found ${path}`);
        releasePostItems.push({
          ...entry,
          downloadUrl: `/projects/${projectId}/repository/blobs/${id}`,
        });
      }

      if (type === "tree" && micromatch.isMatch(path, allowedTrees)) {
        stack.push(path);
      }
    }
  } while (stack.length);

  await asyncPool(
    20,
    releasePostItems,
    downloadFileIfNecessaryFactory(BLOB_CACHE_PATH, targetDir)
  );

  console.log(`Loaded ${releasePostItems.length} items`);
}
