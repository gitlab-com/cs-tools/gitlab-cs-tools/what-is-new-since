# What is new since?

Site is deployed here: https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since

## Features

Parsed from https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data%2Frelease_posts

- [Features]: What are the new features since GitLab release x.y? Filter by releases, tiers, stages and categories!
- [Deprecations]: Which deprecations have been announced when?
- [Stats]: Statistics of the amount of Features introduced every release 

## Usage

- Run via `python3 gitlab_feature_report.py`
- Produces HTML table representation of the features in the `public` folder
- Run via GitLab CI to deploy the feature overview via GitLab pages

[Features]: https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features
[Deprecations]: https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=deprecations
[Stats]: https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=stats
