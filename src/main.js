import "bootstrap/dist/css/bootstrap.min.css";
import "vue-multiselect/dist/vue-multiselect.min.css";
import "dc/dist/style/dc.css";
import "./main.css";
import Vue from "vue";
import Main from "./main.vue";

new Vue({
  el: "#app",
  render(createElement) {
    return createElement(Main);
  },
});
