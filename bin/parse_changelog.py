#!/usr/bin/env python3

import gitlab
import argparse
import re
import json
import markdown
import gzip
from os import path, walk

# version lines are formatted as ## $MAJOR.$MINOR.$PATCH (YYYY-MM-DD)
# where $MAJOR, $MINOR, and $PATCH are integers.
# return the version string only in the function parse_version_line(line)
def parse_version_line(line):
    # Extract version string from the line
    version_match = re.search(r'## (\d+\.\d+\.\d+)', line)
    if version_match:
        return version_match.group(1)
    return None
    
# category lines are formatted as ### $CATEGORY ($number_of_changes changes)
# where $CATEGORY is a string.
# return the category string only in the function parse_category_line(line)
def parse_category_line(line):
    # Extract category string from the line
    category_match = re.search(r'### (\w+)', line)
    if category_match:
        return category_match.group(1)
    return None

# change lines are formatted as - [$title]($commit_url) {by @$contributor} ([merge request]($merge_request_link]) {**GitLab Enterprise Edition**}
# parts in parenthesis are optional and may not be provided for every change
# return a dictionary containing all change data in individual fields in the function parse_change_line(line), except contributor
def parse_change_line(line):
    # Initialize an empty dictionary to store change data
    change_data = {}
    
    # Extract title and commit URL
    # We always need a title. If there is no title, return None
    title_commit_match = re.search(r'\[([^\]]+)\]\(([^)]+)\)', line)
    if title_commit_match:
        change_data['title'] = title_commit_match.group(1)
        commit = title_commit_match.group(2)
        if "@" in commit:
            changeparts = commit.split("@")
            project = changeparts[0]
            sha = changeparts[1]
            change_data["commit_url"] = f"https://gitlab.com/{project}/-/commit/{sha}"
        else:
            change_data['commit_url'] = commit
    else:
        return None
    
    # Extract merge request link
    # merge request links come in two flavors:
    # 1. [merge request](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/123)
    # 2. gitlab-org/gitlab!123
    # just extract the numerical part of the merge request link
    mr_match = re.search(r'\[merge request\]\(([^)]+)\)', line)
    if mr_match:
        mr_match = re.search(r'\d+', mr_match.group(1))
        if mr_match:
            mr_match = mr_match.group(0)
        else:
            mr_match = None
        change_data['merge_request_iid'] = mr_match
    else:
        change_data['merge_request_iid'] = None
    
    # Check if it's a GitLab Enterprise Edition feature
    if "GitLab Enterprise Edition" in line:
        change_data['enterprise_edition'] = True
    else:
        change_data['enterprise_edition'] = False
    
    return change_data

# we want to decorate changes with data from merge requests
# use python gitlab to query a merge request defined by its iid from gitlab.com project 278964
# return a dictionary containing all merge request data in individual fields in the function get_merge_request_data(iid)
def get_merge_request_data(iid):
    project = gl.projects.get(278964)  # GitLab.com project ID for gitlab-org/gitlab
    try:
        print("Querying merge request " + str(iid))
        mr = project.mergerequests.get(iid)
        return {
            'title': mr.title,
            'description': mr.description,
            'labels': mr.labels,
            'web_url': mr.web_url
        }
    except gitlab.exceptions.GitlabGetError:
        print(f"Error: Unable to fetch merge request with IID {iid}")
        return None
    
# we are only interested in certain MR labels
# Labels that are relevant: type::, bug::, feature::, devops::, group::, priority::, severity::, category:, backend, frontend, documentation, security
# return a list of relevant labels in the function get_relevant_labels(mr_data)
def limit_relevant_labels(mr_data):
    relevant_labels = []
    if 'labels' in mr_data:
        for label in mr_data['labels']:
            if (label.startswith(('type::', 'bug::', 'feature::', 'devops::', 'group::', 'priority::', 'severity::', 'category:')) or
                label in ('backend', 'frontend', 'security')):
                relevant_labels.append(label)
    return relevant_labels

# descriptions are markdown strings, but we want to render them in an HTML table
# return a string containing the HTML table for a given markdown description
# clean up the description to hopefully only keep the relevant section
# as not all MRs use the template, this is going to be a bit messy

def clean_description(desc_string):

    desc_string = re.sub("<img.*>", "", desc_string)
    desc_string = re.sub("(<!--.*?-->)", "", desc_string, flags=re.DOTALL)
    desc_string = desc_string.replace("**Please include [cross links](https://handbook.gitlab.com/handbook/communication/#start-with-a-merge-request:~:text=Cross%20link%20issues,alternate%20if%20duplicate.) to any resources that are relevant to this MR**.","")
    desc_string = desc_string.replace("This will give reviewers and future readers helpful context to give an efficient review of the changes introduced.","")
    
    if "## What does this MR do and why?" in desc_string:
        desc_string = desc_string[desc_string.find("## What does this MR do and why?") + len("## What does this MR do and why?"):].strip()
    if "<h2>What does this MR do and why?</h2>" in desc_string:
        desc_string = desc_string[desc_string.find("<h2>What does this MR do and why?</h2>") + len("<h2>What does this MR do and why?</h2>"):].strip()
    if "<h2>MR acceptance checklist</h2>" in desc_string:
        desc_string = desc_string[0:desc_string.find("<h2>MR acceptance checklist</h2>")].strip()
    if "## MR acceptance checklist" in desc_string:
        desc_string = desc_string[0:desc_string.find("## MR acceptance checklist")].strip()
    if "<h2>Screenshots or screen recordings</h2>" in desc_string:
        desc_string = desc_string[0:desc_string.find("<h2>Screenshots or screen recordings</h2>")].strip()
    if "## Screenshots or screen recordings" in desc_string:
        desc_string = desc_string[0:desc_string.find("## Screenshots or screen recordings")].strip()
    desc_string = desc_string.replace('<strong>Please include <a href="https://handbook.gitlab.com/handbook/communication/#start-with-a-merge-request:~:text=Cross%20link%20issues,alternate%20if%20duplicate.">cross links</a> to any resources that are relevant to this MR</strong>',"")
    desc_string = desc_string.replace('This will give reviewers and future readers helpful context to give an efficient review of the changes introduced.',"")
    desc_string = desc_string.replace("<p>\n</p>","")
    desc_string = desc_string.replace("<p>.\n</p>","")
    desc_string = desc_string.replace("<p></p>","")
    desc_string = re.sub("\n+", "\n", desc_string)
    # we don't need a full string here, just enough to get an idea what it does
    #desc_string = desc_string[0:500]
    md = markdown.Markdown()
    desc_string = md.convert(desc_string)
    desc_string = desc_string.replace("<h2>","<strong>")
    desc_string = desc_string.replace("</h2>","</strong><br/>")
    if len(desc_string) > 1000:
        desc_string = desc_string[0:1000]
        desc_string += "..."
    return desc_string

parser = argparse.ArgumentParser(description='Parse GitLab changelog.')
parser.add_argument('token', help="Token to crawl GitLab MRs.")
parser.add_argument('--changelogdata', help="Existing parsed changelog data (gzipped).")
args = parser.parse_args()

gl = gitlab.Gitlab('https://gitlab.com', private_token=args.token)

__dirname__ = path.dirname(path.abspath(__file__))
target_dir = path.join(__dirname__, '..', 'public')

changelogdata = ""
with open("./CHANGELOG.md", "r") as f:
    changelogdata = [line.rstrip() for line in f]

known_changes = {}
if args.changelogdata:
    changedata = ""
    if args.changelogdata.endswith(".gz"):
        with open(args.changelogdata, "rb") as f:
            changedatastring = gzip.decompress(f.read()).decode()
            changedata = json.loads(changedatastring)
    else:
        with open(args.changelogdata, "r") as f:
            changedata = json.load(f)
    for change in changedata:
        if "contributor" in change:
            del change["contributor"]
        known_changes[change['commit_url']] = change
        if "merge_request_iid" in change:
            del change["merge_request_iid"]

current_version = ""
current_category = ""
changes = []
count = 0
for line in changelogdata:
    count += 1
    if line.startswith("## "):
        current_version = parse_version_line(line)     
    elif line.startswith("### "):
        current_category = parse_category_line(line)
    else:
        change = parse_change_line(line)
        if change:
            print(change["title"])
            if change['commit_url'] in known_changes.keys():
                changes.append(known_changes[change['commit_url']])
                continue
            elif "changelogs/archive" in change['title']:
                continue
            else:
                change['version'] = current_version
                change['category'] = current_category
                changes.append(change)
                if change['merge_request_iid']:
                    mr_data = get_merge_request_data(change['merge_request_iid'])
                    if mr_data:
                        mr_data['labels'] = limit_relevant_labels(mr_data)
                        change['merge_request'] = mr_data

# let's do some post processing
for change in changes:
    if "@" in change["commit_url"]:
        changeparts = change["commit_url"].split("@")
        project = changeparts[0]
        sha = changeparts[1]
        change["commit_url"] = f"https://gitlab.com/{project}/-/commit/{sha}"
    # needs to be guarded by known changes
    if "merge_request" in change:
        change["merge_request"]["description"] = clean_description(change["merge_request"]["description"])
    if "merge_request_iid" in change:
        del change["merge_request_iid"]

print("Parsed %s lines of changelog data" % count)
print("Found %s changes" % len(changes))
# Write changes to JSON file

with open(path.join(target_dir, "changelog.json"), 'w') as f:
    json.dump(changes, f, indent=4)

print(f"Changelog data has been written to {path.join(target_dir, "changelog_data.json")}")

compressed_changes = gzip.compress(json.dumps(changes, default=str).encode())
print("Writing changes to gz")
with open("data/changelog.json.gz", "wb") as f:
    f.write(compressed_changes)

print(f"Changelog data has been written to data/changelog.json.gz")