#!/usr/bin/env python3

import gitlab
import argparse
import zipfile
import json
import re
import os
from urllib.parse import urlparse

def get_groupmembers(token, group):
    gl = gitlab.Gitlab("https://gitlab.com/", private_token=token)
    members = []
    group_object = gl.groups.get(group, lazy=True)
    group_members = group_object.members.list(iterator=True)
    for user in group_members:
        members.append(user.username)
    return members

def get_path(issue_url):
    path = issue_url.replace("https://gitlab.com/","").replace("groups/","")
    path = path[0:path.find("/-")]
    return(path)

def get_iid(issue_url):
    issue_url = urlparse(issue_url).path
    iid = issue_url[issue_url.rfind("/")+1:]
    return(iid)

def find_requests_in_issues(gl, features, people):
    requests = []
    print("Scraping notes for %s features" % str(len(features)))
    count = 0
    for feature in features:
        count += 1
        print("Scraping feature %s" % str(count))
        #print(feature)
        #sometimes we are dealing with lists of issues, so treat every url as a list instead
        feature_links = []
        url = feature["issue_url"]
        if type(url) is list:
            feature_links = url
        else:
            feature_links.append(url)
        for issue_or_epic_link in feature_links:
            path = get_path(issue_or_epic_link)
            iid = get_iid(issue_or_epic_link)
            noteable = None
            if "epics" in issue_or_epic_link:
                try:
                    group = gl.groups.get(path, lazy=True)
                    noteable = group.epics.get(iid)
                except Exception as e:
                    print("Could not get group epic %s with iid %s. Skipping." % (path, iid))
                    print(e)
                    continue
            elif "issues" in issue_or_epic_link:
                try:
                    project = gl.projects.get(path, lazy=True)
                    noteable = project.issues.get(iid)
                except Exception as e:
                    print("Could not get project issue %s with iid %s. Skipping." % (path, iid))
                    print(e)
                    continue
            else:
                print("Only handling issues and epics. Not parsing %s." % issue_or_epic_link)
                continue
            # if it doesn't have a customer label, we don't have to look at the notes
            # it's a nice thought but customer labels are not consistently available, so we will search every issue for notes
            # if "customer" not in noteable.labels and "customer +" not in noteable.labels:
            #     continue
            notes = noteable.notes.list(iterator = True)
            for note in notes:
                # only look at team member's notes
                if note.author["username"] in people:
                    salesforce_mentions = re.findall("https://gitlab.my.salesforce.com/[0-9a-zA-Z]+", note.body)
                    if salesforce_mentions:
                        request = {"requester":note.author["username"], "issue":noteable.web_url, "request":noteable.web_url+"#note_"+str(note.id), "customer":salesforce_mentions[0]}
                        requests.append(request)
    return requests

def post_updates(gl, target_project, version, request_list, groupname):
    #get issues first to make sure we only post one issue for a version
    issues = target_project.issues.list(labels=['customer updates'], iterator=True)
    version = version.replace("_",".")
    for issue in issues:
        if issue.title is "%s delivered customer requests for group %s" % (version, groupname):
            print("Issue already exists for version %s: %s. Not posting updates." % (version,issue.web_url))
            return
    print("Posting updates")
    description = compile_update_description(request_list, version)
    update_issue = target_project.issues.create({"title":"%s delivered customer requests for group %s" % (version, groupname),"description": description})
    update_issue.labels = ["customer updates","version::"+version]
    update_issue.state_event = 'close'
    update_issue.save()

def compile_update_description(request_list, version):
    description = "Hey team, I parsed all issues in the latest release for you and checked their notes. Here are the issues and epics that you requested and which were delivered in **GitLab %s**.\n\n" % version
    description += "| Requester | Issue | Customer | Link to comment |\n"
    description += "| - | - | - | - |\n"
    requester_sorted_requests = sorted(request_list, key=lambda d: d['requester'])
    for request in requester_sorted_requests:
        description += "| @%s | %s+ | [Salesforce](%s) | [Link](%s) |\n" % (request["requester"], request["issue"], request["customer"], request["request"])
    return description

parser = argparse.ArgumentParser(description='Parse latest closed issues and create an update issue for feature requesters')
parser.add_argument("readToken", help="Token used to read GitLab group to be contacted")
parser.add_argument("writeToken", help="Token used to write updates to issue")
parser.add_argument("group", help="Group to get members from")
parser.add_argument("groupname", help="Name of the group")
args = parser.parse_args()

#use os.environ to read tokens
#read token to read group members
#write token to write update
#create 2 issues for CSM and SA seperately by running script 2 times

groupmembers = []
groupname = ""

#gitlab masking requirements force me to pad the group ID up to 8 chars. I'll use dots and remove them here
group_id = args.group.replace(".","")
groupmembers = get_groupmembers(args.readToken, group_id)

gl = gitlab.Gitlab("https://gitlab.com/", private_token=args.writeToken)

# get what's new since, as we want to get artifacts from it and open an issue in there
whats_new = gl.projects.get(17379391)

with open("feature_artifacts.zip", "wb") as f:
    features_artifacts = whats_new.artifacts.download(ref_name='main', job='create_feature_report', streamed=True, action=f.write)

with zipfile.ZipFile("feature_artifacts.zip", 'r') as zip_ref:
    zip_ref.extractall("./artifacts")

features = []
bugs = []

with open("./artifacts/public/features.json") as featurefile:
    features = json.load(featurefile)

# latest features first so we can grab the latest version
features.reverse()
latest_version = features[0]["version"]
latest_version_features = []

for feature in features:
    if "issue_url" in feature and feature["version"] == latest_version:
        latest_version_features.append(feature)

with open("./artifacts/public/bugs_improvements.json") as bugfile:
    bugs = json.load(bugfile)

for bug in bugs:
    if "issue_url" in bug and bug["version"] == latest_version:
        latest_version_features.append(bug)

requests = find_requests_in_issues(gl, latest_version_features, groupmembers)
print(requests)

post_updates(gl, whats_new, latest_version, requests, args.groupname)