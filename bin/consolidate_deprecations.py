#!/usr/bin/env python3
import json
import re
import yaml
import argparse
import markdown
import glob
from os import path, walk

def clean_description(desc_string):
    desc_string = re.sub("<img.*>", "", desc_string)
    desc_string = markdown.markdown(desc_string, extensions=['markdown.extensions.tables'])
    return desc_string

def normalizeVersionString(version):
    version = version.split(".")
    major = version[0]
    minor = version[1]
    if len(major) < 2:
        major = "0" + major
    if len(minor) < 2:
        minor = "0" + minor
    return "%s.%s" % (major, minor)

def load_yaml_file(path):
    content = None
    try:
        with open(path, 'r') as content:
            content = yaml.safe_load(content)
    except:
        print("Can not load data from file: %s" % path)
    return content

def crawl_folders(removal_dir):
    removals = []
    for removal_file in glob.glob(path.join(removal_dir, '*.yml')):
        print("Reading file " + removal_file)
        content = load_yaml_file(removal_file)
        if content:
            removals.extend(content)
    return removals

def clean_impact(impact):
    # sometimes impact is an array. If that is the case, get the first element
    if isinstance(impact, list):
        impact = impact[0]
    if impact == "small":
        impact = "low"
    return impact

parser = argparse.ArgumentParser(description='Consolidate deprecation yaml files to filterable list')
args = parser.parse_args()

__dirname__ = path.dirname(path.abspath(__file__))
target_dir = path.join(__dirname__, '..', 'public')

deprecations = []

deprecation_dir = path.join(__dirname__, '..', 'data', 'deprecations')

for deprecation_file in glob.glob(path.join(deprecation_dir, '*.yml')):
    deprecations.extend(load_yaml_file(deprecation_file))

for deprecation in deprecations:
    deprecation["title"] = clean_description(deprecation["title"])
    deprecation["body"] = clean_description(deprecation["body"])
    deprecation["type"] = "Deprecation"
    if deprecation.get("stage"):
        deprecation["stage"] = deprecation["stage"][0].upper() + deprecation["stage"][1:]
    else:
        deprecation["stage"] = "Other"
    deprecation["announcement_milestone"] = normalizeVersionString(deprecation["announcement_milestone"])
    deprecation["removal_milestone"] = normalizeVersionString(deprecation["removal_milestone"])
    if "breaking_change" in deprecation:
        deprecation["breaking_change"] = str(deprecation["breaking_change"])
    else:
        deprecation["breaking_change"] = "False"
    if "impact" in deprecation:
        deprecation["impact"] = clean_impact(deprecation["impact"])
    else:
        deprecation["impact"] = ""

deprecations = sorted(deprecations, key=lambda d: (d['removal_milestone'], d['announcement_milestone']), reverse=True)

with open(path.join(target_dir, 'deprecations.json'), 'w') as outfile:
    json.dump(deprecations, outfile, indent=4, default = str)
