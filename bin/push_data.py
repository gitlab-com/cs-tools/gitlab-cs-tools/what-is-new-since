#!/usr/bin/env python3

import gitlab
import argparse
import base64

def push_data(project, branch):
    changelogdata = ""
    with open('data/changelog.json.gz', 'rb') as f:
        bin_content = f.read()
        changelogdata = base64.b64encode(bin_content).decode('utf-8')
    try:
        data = {
            'branch': branch,
            'commit_message': "[ci skip] push changelog data",
            'actions': [
                {
                    'action': 'update',
                    'file_path': 'data/changelog.json.gz',
                    'content': changelogdata,
                    'encoding': 'base64'
                }
            ]
        }

        commit = project.commits.create(data)
    except Exception as e:
        raise e

parser = argparse.ArgumentParser(description='Push data to project')
parser.add_argument('token', help='API token able to push to the project repo')
parser.add_argument('gitlab', help='GitLab instance url')
parser.add_argument('project', help='ID of the project to push to')
parser.add_argument('branch', help='The branch to push to')

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)
project = gl.projects.get(args.project)

try:
    print("Updating data files")
    push_data(project, args.branch)
except Exception as e:
    print("Could not push file to repo")
    print(e)
    exit(1)