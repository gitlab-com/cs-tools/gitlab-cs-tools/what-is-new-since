#!/usr/bin/env node

import { recurseRepoAndDownload } from "../lib/recurse_repo_and_download.js";

async function main() {
  const GITLAB_ID = "278964";

  await recurseRepoAndDownload({
    projectId: GITLAB_ID,
    startTree: ["."],
    allowedBlobs: ["CHANGELOG.md"],
    allowedTrees: ["."],
  });
}

main()
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
