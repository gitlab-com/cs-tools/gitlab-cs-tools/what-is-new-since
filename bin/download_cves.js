#!/usr/bin/env node
import { join } from "node:path";

import { recurseRepoAndDownload } from "../lib/recurse_repo_and_download.js";
import { ROOT_DIR } from "../lib/constants.js";

async function main() {
  const targetDir = join(ROOT_DIR, "./cves/");

  const GITLAB_ID = "18741849";

  await recurseRepoAndDownload({
    projectId: GITLAB_ID,
    startTree: [""],
    allowedBlobs: ["**/*.json"],
    targetDir,
  });
}

main()
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
