#!/usr/bin/env node

import { recurseRepoAndDownload } from "../lib/recurse_repo_and_download.js";

async function main() {
  const GITLAB_ID = "7764";

  await recurseRepoAndDownload({
    projectId: GITLAB_ID,
    startTree: ["data"],
    allowedTrees: ["data/release_posts/**"],
    allowedBlobs: ["data/release_posts/**/*.yml", "data/releases.yml"],
  });
}

main()
  .then(() => {
    console.log("success");
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
