PyYAML==5.1.2
markdown==3.2.1
python-dateutil==2.8.1
python-gitlab==3.12.0
semantic-version==2.9.0